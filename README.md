# Personal Portfolio Webpage

Responsive personal portfolio page built using HTML, CSS, JS and Bootstrap. This was created for front-end development class assignment.

You can see the page [here](https://cgfportfolio.netlify.com/).
